Vue.component("myrepeater",{
	props:["list"],
	template:`<div>
				<slot name="a"></slot>
				<hr/>
				<slot name="b"></slot>
				<slot v-for="item in list" :item="item"></slot>
			</div>`
})