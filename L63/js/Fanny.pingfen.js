Vue.component("pingfen", {
	props:["c","value","color"],
	template: `<div class="star-container">
				<div @click="score = i" @mouseover="tempscore=i" @mouseout="tempscore = score" v-for="i in count" :class="i<=tempscore ? 'star' : 'star-o'"></div>
			</div>
	`,
	data() {
		return {
			count: 5,
			score: 3,
			tempscore: ''
		}
	},
	watch:{
		score(val){
			this.$emit("input",val)
		}
		
	},
	mounted(){
		if(this.c && !isNaN(this.c)){
			var c = parseInt(this.c);
			if (c>0) {
				this.count = c
			}
			
		}
		if(this.value && !isNaN(this.value)){
			var value = parseInt(this.score);
			if (value>0) {
				this.score = value
			}
			
		}
		
		if (this.score > this.count) {
			this.score = this.count
		}
		
		this.tempscore = this.value
	}
})