Vue.component("al-table",{
    props:{
        data:{
            required:true
        }
    },

    data(){
        return {
            cols:[],
            rows:[]
        }
    },
    mounted(){
        this.cols=this.data.cols
        for (let i = 0; i < this.cols.length; i++) {
            this.cols[i].sortType = "normal"
        }
        this.rows=this.data.rows

    },
    methods:{
        order(type,key){
            this.cols.forEach((item,i)=>{
                if (item.key !== key){
                    item.sortType = "normal"
                } else {
                    item.sortType = type
                    this.$set(this.cols,i,item)
                }
            })


           this.rows.sort((a,b)=>{
               if (type === 'asc'){
                   return  a[key]>b[key] ? 1 : -1
               } else {
                   return  a[key]>b[key] ? -1 : 1
               }
           })

        }

    },
    template:
        `
        <table>
            <tr>
                <th v-for="(item,index) in cols">
                {{item.title}}
                <span v-if="item.sortable">
                    <label @click="order('asc',item.key)" :class="{active:item.sortType === 'asc'}" class="orderarrow">↑</label>
                    <label @click="order('desc',item.key)" :class="{active:item.sortType === 'desc'}" class="orderarrow">↓</label>
                </span>
                </th>
            </tr>
            <tr v-for="item in rows">
                <td>{{item.id}}</td>
                <td>{{item.name}}</td>
                <td>{{item.age}}</td>
                <td>{{item.score}}</td>
            </tr>
        
        </table>
    
        `
})